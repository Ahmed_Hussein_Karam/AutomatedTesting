<?php
use \Codeception\Step\Argument\PasswordArgument;
use \Codeception\Example;

class CreatePagesCest
{
    public function _before(AcceptanceTester $I)
    {
        $I->wantTo('Login to the website');

        $I->amOnUrl("http://techinterview.otgs-yt.tk/");
        $I->click('//*[@id="meta-2"]/ul/li[1]/a');

        $I->amGoingTo('Enter my credentials');
        $I->fillField('#user_login','admin');
        $I->fillField('#user_pass',new PasswordArgument('techINotg$'));
        $I->click('#wp-submit');
    }

    /**
     * @dataProvider pageProvider
     */
    public function createPages(AcceptanceTester $I, Example $example)
    {
        $I->wantTo('create a page and see the content in the front end');

        $I->amGoingTo('create a page');
        $I->moveMouseOver('//*[@id="menu-pages"]/a/div[3]');
        $I->click('//*[@id="menu-pages"]/ul/li[3]/a');
        $I->fillField('#title',$example['title']);
        $I->fillField('#content',$example['content']);
        sleep(2);
        $I->click('#publish');

        $I->expectTo('find the page content in the front end');
        $I->click('//*[@id="sample-permalink"]/a');
        $I->see($example['content'], 'p');
    }

    public function _after(AcceptanceTester $I)
    {
        $I->wantTo('Clean up database to remove testing footprints');

        $I->amGoingTo('Delete created page');
        $I->moveBack();
        $I->click('//*[@id="delete-action"]/a');
        $I->click('//*[@id="wpbody-content"]/div[3]/ul/li[4]/a');
        $I->click('#delete_all');
    }

    /**
     * @return array
     */
    protected function pageProvider() // alternatively, if you want the function to be public, be sure to prefix it with `_`
    {
        return [
            ['title'=>'First Title', 'content'=>'First Content'],
            ['title'=>'Second Title', 'content'=>'Second Content'],
            ['title'=>'Third Title', 'content'=>'Third Content']
        ];
    }
}
